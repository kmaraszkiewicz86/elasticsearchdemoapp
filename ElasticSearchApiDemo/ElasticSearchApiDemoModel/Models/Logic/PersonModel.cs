﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ElasticSearchApiDemoModel.Models.Logic
{
    public class SearchModelResponse
    {
        public SearchModelSecondLevel Hits { get; set; }
    }

    public class SearchModelSecondLevel
    {
        public SearchModelThirdLevel Hits { get; set; }
    }

    public class SearchModelThirdLevel
    {
        [JsonPropertyName("_source")]
        public PersonModel PersonModel { get; set; }
    }

	public class PersonModel
	{
        public int Id { get; set; }

        public string Profession { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string Lastname { get; set; }
    }
}

