namespace ElasticSearchApiDemoModel.Models.Logic;

public class SearchModel
{
    public string Name { get; set; }

    public string Surname { get; set; }
}