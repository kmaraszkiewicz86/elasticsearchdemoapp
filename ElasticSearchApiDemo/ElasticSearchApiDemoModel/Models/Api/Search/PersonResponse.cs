namespace ElasticSearchApiDemoModel.Models.Api.Search;

public class PersonResponse
{
    public int Id { get; }
    
    public string FirstName { get; }

    public string LastName { get; }

    public string Email { get; }

    public string Profession { get; }

    public PersonResponse(int id, string firstName, string lastName, string email, string profession)
    {
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        Profession = profession;
    }
}