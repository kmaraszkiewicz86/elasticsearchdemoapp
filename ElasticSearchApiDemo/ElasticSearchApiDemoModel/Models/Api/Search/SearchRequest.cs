namespace ElasticSearchApiDemoModel.Models.Api.Search;

public class SearchRequest
{
    public string Name { get; set; }

    public string Surname { get; set; }
}