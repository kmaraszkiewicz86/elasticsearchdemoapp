namespace ElasticSearchApiDemoModel.Models.Api.Search;

public abstract class ResultBase
{
    public string ErrorMessage { get; } = string.Empty;

    public List<string> ValidationErrors { get; } = new ();

    public bool IsValid => string.IsNullOrWhiteSpace(ErrorMessage) && ValidationErrors.Count == 0;

    public ResultBase()
    {
        
    }
    
    public ResultBase(string errorMessage)
    {
        ErrorMessage = errorMessage;
    }

    public ResultBase(List<string> validationErrors)
    {
        ValidationErrors = validationErrors;
    }
}