using FluentValidation.Results;

namespace ElasticSearchApiDemoModel.Models.Api.Search;

public class ResultWithModel<TModel> : ResultBase
{
    public TModel Model { get; }

    private ResultWithModel(TModel model)
    {
        Model = model;
    }

    private ResultWithModel(string errorMessage) : base(errorMessage)
    {
        Model = default;
    }

    private ResultWithModel(List<string> validationErrors) : base(validationErrors)
    {
        
    }

    public static ResultWithModel<TModel> Ok(TModel model) => new (model);

    public static ResultWithModel<TModel> Fail(IEnumerable<ValidationFailure> errors) =>
        new(errors.Select(e => e.ErrorMessage).ToList());

    public static ResultWithModel<TModel> Fail(string errorMessage) => new (errorMessage);
}