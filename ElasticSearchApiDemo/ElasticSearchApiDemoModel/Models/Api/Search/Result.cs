namespace ElasticSearchApiDemoModel.Models.Api.Search;

public class Result : ResultBase
{
    public Result()
    {
        
    }
    
    public Result(string errorMessage) : base(errorMessage)
    {
        
    }
    
    public static Result Ok() => new ();

    public static Result Fail(string errorMessage) => new (errorMessage);
}