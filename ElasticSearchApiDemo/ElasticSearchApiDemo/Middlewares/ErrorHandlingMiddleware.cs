using ElasticSearchApiDemoModel.Models.Api.Search;
using System.Text.Json;

namespace ElasticSearchApiDemo.Middlewares;

public class ErrorHandlingMiddleware
{
    private readonly RequestDelegate _next;

    private readonly ILogger<ErrorHandlingMiddleware> _logger;

    public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task Invoke(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception e)
        {
            var errorJsonString = JsonSerializer.Serialize(new Result(e.Message));

            _logger.LogError(e, "Unhandled error occured");

            httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            httpContext.Response.ContentType = "application/json";
            
            await httpContext.Response.WriteAsync(errorJsonString);
        }
    }
}