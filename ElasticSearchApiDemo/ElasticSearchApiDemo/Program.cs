using ElasticSearchApiDemo.Middlewares;
using ElasticSearchApiDemoLogic.Extensions;
using Elasticsearch;
using Serilog;
using Serilog.Configuration;
using Serilog.Sinks.Elasticsearch;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

ConfigureLogging();

// Add services to the container.

builder.Host.UseSerilog();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.ConfigureServices();

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(builder => builder.AllowAnyHeader()
            .AllowAnyMethod()
            .AllowAnyOrigin());
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors();

app.UseMiddleware<ErrorHandlingMiddleware>();

app.UseAuthorization();

app.MapControllers();

app.Run();

static void ConfigureLogging()
{
    var envinronment = Environment.GetEnvironmentVariable("APSNETCORE_ENVIRONMENT");

    var configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .Build();

    Log.Logger = new LoggerConfiguration()
        .Enrich.FromLogContext()
        .WriteTo.Console()
        .WriteTo.Elasticsearch(ConfigureElasticsearchSinkOptions(configuration))
        .ReadFrom.Configuration(configuration)
        .CreateLogger();
}

static ElasticsearchSinkOptions ConfigureElasticsearchSinkOptions(
    IConfiguration configuration)
{
    var indexKey = Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace(".", "-");

    return new ElasticsearchSinkOptions(new Uri(configuration["ElasticSearch:Url"]))
    {
        AutoRegisterTemplate = true,
        IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower()}"
    };
}