using ElasticSearchApiDemoLogic.Handlers.Queries;
using ElasticSearchApiDemoModel.Models.Api.Search;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ElasticSearchApiDemo.Controllers;

[ApiController]
[Route("[controller]")]
public class SearchController : ControllerBase
{
    private readonly ILogger<SearchController> _logger;
    private readonly IMediator _mediator; 

    public SearchController(ILogger<SearchController> logger, IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<ActionResult<ResultWithModel<IEnumerable<PersonResponse>>>> Search(SearchRequest searchRequest, CancellationToken cancellationToken)
    {
        _logger.LogInformation($"Starting searching elasticsearch with data: ",
            JsonConvert.SerializeObject(searchRequest));

        ResultWithModel<IEnumerable<PersonResponse>> result = await _mediator.Send(
            new GetSearchQuery(searchRequest.Name, searchRequest.Surname),
            cancellationToken);

        if (result.IsValid) return Ok(result);

        _logger.LogInformation($"Data from search process: {JsonConvert.SerializeObject(result)}");

        return BadRequest(result);
    }
}