using ElasticSearchApiInfrastructure.Services;
using ElasticSearchApiInfrastructure.Services.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace ElasticSearchApiInfrastructure.Extensions;

public static class ServiceCollectionExtension
{
    public static IServiceCollection ConfigureInfrastructureServices(this IServiceCollection collection)
    {
        collection.AddHttpClient<ISearchService, SearchService>(client =>
        {
            client.BaseAddress = new Uri("http://localhost:9200/");
        });

        return collection;
    }
}