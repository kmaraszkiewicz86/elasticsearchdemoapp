namespace ElasticSearchApiInfrastructure.Services;

public interface ISearchService
{
    Task<HttpResponseMessage> Search(string name, string surname, CancellationToken cancellationToken);
}