using System.Text;

namespace ElasticSearchApiInfrastructure.Services.Implementations;

public class SearchService : ISearchService
{
    private readonly HttpClient _httpClient;

    public SearchService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<HttpResponseMessage> Search(string name, string surname, CancellationToken cancellationToken)
    {
        var query = await File.ReadAllTextAsync("./bin/debug/net6.0/Queries/elsticsearchQuery.json", cancellationToken);
        query = query.Replace("{0}", name).Replace("{1}", surname);

        var stringContent = new StringContent(query, Encoding.UTF8, "application/json");

        return await _httpClient.PostAsync("test/_search", stringContent, cancellationToken);
    }
}