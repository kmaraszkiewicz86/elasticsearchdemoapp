using ElasticSearchApiDemoLogic.Handlers.Queries;
using FluentValidation;

namespace ElasticSearchApiDemoLogic.Validators;

public class SearchQueryValidator : AbstractValidator<GetSearchQuery>
{
    public SearchQueryValidator()
    {
        RuleFor(x => x.Name)
            .NotEmpty()
            .MaximumLength(100);

        RuleFor(x => x.Surname)
            .NotEmpty()
            .MaximumLength(100);
    }
}