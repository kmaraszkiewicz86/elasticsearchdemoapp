using ElasticSearchApiDemoModel.Models.Api.Search;
using ElasticSearchApiDemoModel.Models.Logic;
using ElasticSearchApiInfrastructure.Services;
using FluentValidation;
using MediatR;
using System.Text.Json;
using Newtonsoft.Json.Linq;

namespace ElasticSearchApiDemoLogic.Handlers.Queries;

public class GetSearchQueryHandler : IRequestHandler<GetSearchQuery, ResultWithModel<IEnumerable<PersonResponse>>>
{
    private readonly IValidator<GetSearchQuery> _validator;

    private readonly ISearchService _searchService;

    public GetSearchQueryHandler(IValidator<GetSearchQuery> validator, ISearchService searchService)
    {
        _validator = validator;
        _searchService = searchService;
    }

    public async Task<ResultWithModel<IEnumerable<PersonResponse>>> Handle(GetSearchQuery request, CancellationToken cancellationToken)
    {
        var result = await _validator.ValidateAsync(request, cancellationToken);

        if (!result.IsValid)
        {
            return ResultWithModel<IEnumerable<PersonResponse>>.Fail(result.Errors);
        }

        var response = await _searchService.Search(request.Name, request.Surname, cancellationToken);

        if (!response.IsSuccessStatusCode)
        {
            return ResultWithModel<IEnumerable<PersonResponse>>.Fail(
                await response.Content.ReadAsStringAsync(cancellationToken));
        }

        string responseString = await response.Content.ReadAsStringAsync();

        JObject objectFromJson = JObject.Parse(responseString);

        var peopleModels = objectFromJson["hits"]["hits"];

        var reponseItems = new List<PersonResponse>();

        if (peopleModels.Count() > 0)
        {
            foreach (var personResponseModel in peopleModels)
            {
                var personJson = personResponseModel["_source"];

                PersonResponse personResponse = personJson.ToObject<PersonResponse>();

                reponseItems.Add(personResponse);
            }
        }

        return ResultWithModel<IEnumerable<PersonResponse>>.Ok(reponseItems);
    }
}