using ElasticSearchApiDemoModel.Models.Api.Search;
using MediatR;

namespace ElasticSearchApiDemoLogic.Handlers.Queries;

public class GetSearchQuery : IRequest<ResultWithModel<IEnumerable<PersonResponse>>>
{
    public string Name { get; }

    public string Surname { get; }

    public GetSearchQuery(string name, string surname)
    {
        Name = name;
        Surname = surname;
    }
}