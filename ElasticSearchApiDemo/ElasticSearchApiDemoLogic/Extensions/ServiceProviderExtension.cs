using ElasticSearchApiDemoLogic.Validators;
using ElasticSearchApiInfrastructure.Extensions;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace ElasticSearchApiDemoLogic.Extensions;

public static class ServiceProviderExtension
{
    public static IServiceCollection ConfigureServices(this IServiceCollection services)
    {
        services.AddAutoMapper(typeof(ServiceProviderExtension));
        services.AddMediatR(typeof(ServiceProviderExtension));
        
        services.AddValidatorsFromAssemblyContaining<SearchQueryValidator>();

        services.ConfigureInfrastructureServices();
        
        return services;
    }
}