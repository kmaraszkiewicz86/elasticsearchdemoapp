export interface Person {
    firstname: string;
    lastname: string;
    profession: string;
    email: string;
}

export interface Result<TType> {
    model: TType;
    errorMessage: string;
    validationErrors: Record<string, string>;
    isValid: boolean;
}