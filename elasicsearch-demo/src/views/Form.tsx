import React, {useState} from "react";
import { sendSearchRequest } from "../services/formViewService"
import { Person, Result } from "../types/shared";

export interface FormViewProps {
    handleSearch: (data: Result<Person[]>) => void 
}

const FormView = (props: FormViewProps) => {

    const { handleSearch } = props;

    const [name, setName] = useState("")
    const [surname, setSurname] = useState("")
    

    return (<>
        <input type={"text"} value={name} onChange={event => setName(event.target.value)}  placeholder={"Szukaj po nazwie"} />
        <input type={"text"} value={surname} onChange={event => setSurname(event.target.value)} placeholder={"Szukaj po nazwisku"}/>
        <button type={"button"} onClick={async e => await sendSearchRequest(e, name, surname, handleSearch)} >
            Szukaj
        </button>
    </>)
}

export { FormView }