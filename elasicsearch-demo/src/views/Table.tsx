import { table } from "console";
import React from "react";
import { Person, Result } from "../types/shared";

interface TableView {
    people: Result<Person[]>
}

const TableView = (props: TableView) => {

    const { people } = props;

    if (!people.isValid && people.validationErrors !== undefined && Object.keys(people.validationErrors).length > 0) {
        return (<table>
            <tr>
                <th>
                    <ul>
                        {Object.keys(people.validationErrors).map(key => 
                           <li>{people.validationErrors[key]}</li> 
                        )}
                    </ul>
                </th>
            </tr>
        </table>)
    }

    if (!people.isValid) {
        return (<table>
            <tr>
                <th>
                    {people.errorMessage}
                </th>
            </tr>
        </table>)
    }

    if (people.model === undefined || people.model == null || people.model.length === 0) {
        return (<table>
            <tr>
                <th>Brak danych</th>
            </tr>
        </table>)
    }

    return (<table>
            <tfoot>
                {people.model.map(person => 
                    <tr>
                        <td>
                            {person.firstname}
                        </td>
                        <td>
                            {person.lastname}
                        </td>
                        <td>
                            {person.email}
                        </td>
                    </tr>  
                )}
            </tfoot>
        </table>)
}
export { TableView }