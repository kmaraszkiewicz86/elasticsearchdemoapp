import { Person, Result } from "../types/shared";

const sendSearchRequest = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, name: string, surname: string, handleSearch: (data: Result<Person[]>) => void) => {
        
    let bodyData = new FormData();
    bodyData.append('firstname', name);
    bodyData.append('surname', surname);
    
    const response = await fetch("https://localhost:7165/Search", {
        method: "post",
        body: JSON.stringify({
            name: name,
            surname: surname
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    const people: Result<Person[]> = await response.json();

    handleSearch(people);
    
    e.preventDefault()
}

export { sendSearchRequest }