import { useState } from 'react';
import { FormView } from "./views/Form"
import { TableView } from './views/Table';
import { Person, Result } from "./types/shared";

const TestView = () => {
  return (<>
    <h1>test1</h1>
    <p>test2</p>
  </>)
}

const  App = () => {

  const [people, setPeople] = useState<Result<Person[]>>({
    model: [],
    errorMessage: "",
    isValid: false,
    validationErrors: {}
  })

  return (
      <div className="App">
        <FormView handleSearch={people => setPeople(people)} />
        <TableView people={people} />
      </div>
  );
}

export default App;
